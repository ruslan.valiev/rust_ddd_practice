/**
Добывает поля структуры и помещает в HashMap
# Example
```
# use std::collections::HashMap;

trait MyTraitForTestStruct{
    fn enum_into_raw_test_struct(&self) -> HashMap<String,String> {
       use rust_ddd_sandbox_new::enum_into_raw;enum_into_raw!(&self, TestStruct, field_a, field_b)
    }
}
# fn main() {}
```
**/

#[macro_export]
macro_rules! enum_into_raw {
    ($enum_name:ident,$enum_value:tt,$($field_name:ident),*) => {{
        let mut hm = HashMap::new();
        match $enum_name {
            $enum_value { $($field_name),* } => {$(hm.insert(stringify!($field_name).to_string(), $field_name.clone());)*}
            _ => panic!(
                "Tried to use a {:?} as a {}",
                $enum_name,
                stringify!($enum_value)
            ),
        }
        hm
    }};
}

macro_rules! my_macro {
    (struct $name:ident {
        $($field_name:ident: $field_type:ty,)*
    }) => {
        struct $name {
            $($field_name: $field_type,)*
        }

        impl $name {
            // This is purely an example—not a good one.
            fn get_field_names() -> Vec<&'static str> {
                vec![$(stringify!($field_name)),*]
            }
        }
    }
}

macro_rules! into_raw(
    ($value:expr, $expected_type:tt) => {
        match $value {
            $expected_type(i) => i,
            value => panic!("Tried to use a {:?} as a {}", value, stringify!($expected_type)),
        }
    }
);
fn main() {}
