use crate::shop::config::DomainEventEnum;
use enum_dispatch::enum_dispatch;
use std::fmt::Debug;

#[enum_dispatch]
pub trait DomainEventPublisher: Debug + erased_serde::Serialize + Clone {
    fn publish(&mut self, events: Vec<DomainEventEnum>);
}
