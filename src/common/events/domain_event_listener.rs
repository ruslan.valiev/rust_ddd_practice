use crate::common::types::base::domain_event::*;

pub trait DomainEventListener: DomainEventTrait {
    fn event_type() -> dyn DomainEventTrait;
    fn handle(event: dyn DomainEventTrait);
}

//todo разобраться с KClass
// pub struct KClass<T> {
//     t: T,
// }
