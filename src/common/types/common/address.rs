#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Address {
    street: String,
    building: i64,
}

impl Address {
    pub fn new(street: String, building: i64) -> Result<Self, AddressError> {
        if street.is_empty() || street == *" " {
            Err(AddressError::EmptyString)
        } else if building <= 0 {
            Err(AddressError::NonPositiveBuilding)
        } else {
            Ok(Self { street, building })
        }
    }
}

impl Address {
    pub fn street_to_string_value(&self) -> Result<&String, AddressError> {
        Ok(&self.street)
    }

    pub fn building_to_int_value(&self) -> Result<&i64, AddressError> {
        let building = &self.building;

        if building < &0 {
            return Err(AddressError::NonPositiveBuilding);
        }
        Ok(building)
    }

    pub fn validate_address(&self) {
        match self.building_to_int_value() {
            Ok(s) => println!("Проверка номера здания {} прошла успешно", s),
            Err(err) => {
                eprintln!("Error: {}", err)
            }
        }
        match self.street_to_string_value() {
            Ok(s) => println!("Проверка названия улицы {} прошла успешно", s),
            Err(err) => {
                eprintln!("Error: {}", err)
            }
        }
    }
}

/// WordCountError enumerates all possible errors returned by this library.
///
/// ```
/// #[derive(Error, Debug)]
/// pub enum AddressError {
///
/// ```
/// Represents an empty source. For example, an empty text file being given
/// as input to `count_words()`.
/// ```
///     #[error("Source contains no data")]
///     EmptySource,
///```
///
/// Represents a failure to read from input.
///```
///     #[error("Read error")]
///     ReadError { source: std::io::Error },
/// ```
///
/// Represents all other cases of `std::io::Error`.
/// ```
///     #[error(transparent)]
///     IOError(#[from] std::io::Error),
/// }
/// ```
#[derive(thiserror::Error, Debug, PartialEq, Clone)]
pub enum AddressError {
    #[error("Название улицы не может быть пустым")]
    EmptyString,

    #[error("Номер дома не может быть отрицательным")]
    NonPositiveBuilding,
}
