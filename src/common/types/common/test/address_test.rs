#![allow(non_snake_case)]
use crate::common::types::common::address::{Address, AddressError};
use rstest::rstest;

#[allow(non_snake_case)]
#[test]
fn create_address__success() {
    let street = "Street".to_string();
    let building = 15;

    let address = Address::new(street.to_owned(), building).unwrap();
    assert_eq!(address.building_to_int_value().unwrap(), &building);
    assert_eq!(address.street_to_string_value().unwrap(), &street);
}

#[rstest]
#[case("")]
#[case(" ")]
fn create_address__empty_string(#[case] street: String) {
    let result = Address::new(street, 1);
    assert_eq!(result, Err(AddressError::EmptyString));
}

#[rstest]
#[case(0)]
#[case(-1)]
fn create_address__non_positive_building(#[case] building: i64) {
    let result = Address::new("Street".to_string(), building);
    assert_eq!(result, Err(AddressError::NonPositiveBuilding));
}
