#![allow(non_snake_case)]
use crate::common::types::common::count::{Count, CountError};
use rstest::rstest;

#[rstest]
#[case(0)]
#[case(1)]
#[case(i32::MAX)]
fn create_count__success(#[case] value: i32) {
    let count = Count::new(value).unwrap();
    assert_eq!(count.to_i32_value(), value);
}

#[test]
fn create_count__one() {
    let result = Count::one().unwrap();
    assert_eq!(result.to_i32_value(), 1);
}

#[test]
fn create_count__negative_value() {
    let result = Count::new(-1);
    assert_eq!(result, Err(CountError::NegativeValueError));
}

#[test]
fn increment__success() {
    let count = Count::new(1).unwrap();
    let increment = count.increment();
    assert_eq!(increment, Count::new(count.to_i32_value() + 1));
}

#[test]
fn increment__max_value_reached() {
    let count = Count::new(i32::MAX).unwrap();
    let result = count.increment();
    assert_eq!(result, Err(CountError::MaxValueReachedError));
}

#[rstest]
#[case(1)]
#[case(i32::MAX)]
fn decrement__success(#[case] value: i32) {
    let count = Count::new(value).unwrap();
    let decrement = count.decrement().unwrap();
    assert_eq!(decrement, Count::new(count.to_i32_value() - 1).unwrap());
}

#[test]
fn decrement__min_value_reached() {
    let count = Count::new(0).unwrap();
    let result = count.decrement();
    assert_eq!(result, Err(CountError::MinValueReachedError));
}

#[test]
fn check_is_min_value__true() {
    let count = Count::new(0).unwrap();
    assert!(count.is_min());
}

#[test]
fn check_is_min_value__false() {
    let count = Count::new(1).unwrap();
    assert!(!count.is_min());
}

#[test]
fn check_is_max_value__true() {
    let count = Count::new(i32::MAX).unwrap();
    assert!(count.is_max());
}

#[test]
fn check_is_max_value__false() {
    let count = Count::new(1).unwrap();
    assert!(!count.is_max());
}
