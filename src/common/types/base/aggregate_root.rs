use crate::common::types::base::domain_entity::*;
use derive_new::new;

#[derive(new, Clone, Debug)]
pub struct AggregateRoot<T> {
    pub id: T,
    pub version: Version,
}

// impl<T> AggregateRoot<T> {}
// abstract class AggregateRoot<T>(id: T, version: Version) : DomainEntity<T>(id, version)
