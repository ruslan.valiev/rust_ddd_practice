use crate::common::types::base::domain_entity::Version;
use rand::thread_rng;
use rand::Rng;
use std::convert::From;

#[allow(non_snake_case)]
#[test]
fn new_id__check_version_is_zero() {
    let first_version = Version::new();
    let second_version = Version::new();

    assert_eq!(
        first_version.to_long_value(),
        second_version.to_long_value()
    );
    assert_eq!(first_version.to_long_value(), 0)
}

#[test]
fn restore_from_long() {
    let long: i64 = thread_rng().gen();
    let version = Version::from(long);
    assert_eq!(version.to_long_value(), long)
}

#[allow(non_snake_case)]
#[test]
fn increment_counter__value_is_plus_1() {
    let long: i64 = thread_rng().gen();
    let version = Version::from(long);
    let incremented = version.next();
    assert_eq!(incremented.to_long_value(), long + 1)
}

#[allow(non_snake_case)]
#[test]
fn the_same_value_should_be_equals() {
    let long: i64 = thread_rng().gen();
    let first = Version::from(long);
    let second = Version::from(long);
    assert_eq!(first, second)
}

#[allow(non_snake_case)]
#[test]
fn previous_version_should_be_current_minus_1() {
    let long: i64 = thread_rng().gen();
    let version = Version::from(long);
    let previous = version.previous();
    assert_eq!(previous.to_long_value(), version.to_long_value() - 1)
}
