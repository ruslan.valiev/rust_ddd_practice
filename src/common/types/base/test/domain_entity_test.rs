#![allow(non_snake_case)]

use crate::common::types::base::domain_entity::*;
use crate::common::types::base::domain_event::*;
use crate::shop::config::DomainEventEnum;
use crate::shop::domain::menu::meal_id::MealId;
use crate::shop::domain::menu::test::fixtures::rnd_meal_id;
use derive_new::new;
use mockall::Any;
use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use time::OffsetDateTime;

#[test]
fn produce_event__event_stack_is_clean() {
    let id = 1i64;
    let version = Version::new();
    let mut entity = TestEntity::new(id, version, Vec::new());
    entity.do_something();
    assert_eq!(entity.id, id);
    assert_eq!(entity.version, version.next());
    let events = entity.pop_events();
    assert_eq!(events.len(), 1);
    let type_name = events[0].type_name();
    assert_eq!(
        type_name,
        "rust_ddd_sandbox_new::shop::config::DomainEventEnum".to_string()
    );
}

#[test]
fn version_is_incremented_only_single_times_after_altering_entity() {
    let id = 1;
    let version = Version::new();
    let mut entity = TestEntity::new(id, version, Vec::new());

    for _i in 0..9 {
        entity.do_something();
    }

    assert_eq!(entity.version, version.next())
}

#[test]
fn version_is_incremented_after_popping_events() {
    let id = 1_i64;
    let version = Version::new();
    let mut entity = TestEntity::new(id, version, vec![]);
    entity.do_something();
    entity.pop_events();
    entity.do_something();
    assert_eq!(entity.version, version.next().next());
}

#[derive(new, Debug, Clone)]
pub struct TestEntity {
    id: i64,
    version: Version,
    #[new(value:Vec<TestEventEnum> = Vec::new())]
    events: Vec<DomainEventEnum>,
}

impl TestEntity {
    fn do_something(&mut self) {
        let event: DomainEventEnum = TestEvent::new(rnd_meal_id()).into();
        self.add_event(event)
    }
}

impl DomainEntityTrait for TestEntity {
    fn add_event(&mut self, event: DomainEventEnum) {
        if self.events.is_empty() {
            self.version = self.version.next();
        }
        self.events.push(event)
    }
    fn pop_events(&mut self) -> Vec<DomainEventEnum> {
        let res = self.events.clone();
        self.events = Vec::new();
        res
    }
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Debug, Clone, Copy)]
pub struct TestEvent {
    pub(crate) id: EventId,
    pub(crate) created: OffsetDateTime,
}

impl TestEvent {
    fn new(id: MealId) -> Self {
        Self {
            id: EventId::new(),
            created: OffsetDateTime::now_utc(),
        }
    }
}

impl DomainEventTrait for TestEvent {}
