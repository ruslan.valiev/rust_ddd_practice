use derive_new::new;
use enum_dispatch::enum_dispatch;
use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use time::OffsetDateTime;
use uuid::Uuid;

#[derive(PartialEq, Eq, Serialize, Deserialize, Debug, Clone, Copy)]
pub struct DomainEvent {
    id: EventId,
    created: OffsetDateTime,
}

impl DomainEvent {
    pub fn new() -> Self {
        Self {
            id: EventId::new(),
            created: OffsetDateTime::now_utc(),
        }
    }
}

impl Default for DomainEvent {
    fn default() -> Self {
        Self::new()
    }
}

#[enum_dispatch]
pub trait DomainEventTrait: Debug + erased_serde::Serialize {}
// todo возможно понадобится
// serialize_trait_object!(DomainEventTrait<T>);

impl DomainEventTrait for DomainEvent {}

#[derive(new, PartialEq, Eq, Serialize, Deserialize, Debug, Clone, Default, Copy)]
pub struct EventId {
    #[new(value = "Uuid::new_v4()")]
    pub value: Uuid,
}
