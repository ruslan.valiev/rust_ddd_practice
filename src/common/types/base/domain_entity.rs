use crate::common::types::base::value_object::ValueObject;
use crate::shop::config::DomainEventEnum;
use core::fmt::Debug;
use derive_new::new;
use enum_dispatch::enum_dispatch;
use serde::{Deserialize, Serialize};
use std::convert::From;

//todo Добавить ивенты

#[derive(new, Serialize, Debug, Clone, Default)]
pub struct DomainEntity<T> {
    pub id: T,
    #[new(value = "Version::new()")]
    pub version: Version,
    #[new(value = "vec![]")]
    pub events: Vec<DomainEventEnum>,
}

impl<T> DomainEntity<T> {}

#[derive(new, Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq, Default)]
pub struct Version {
    #[new(value = "0_i64")]
    value: i64,
}

impl Version {
    pub fn to_long_value(&self) -> i64 {
        self.value
    }
    pub fn next(&self) -> Version {
        Version {
            value: &self.value + 1,
        }
    }
    pub fn previous(&self) -> Version {
        Version {
            value: &self.value - 1,
        }
    }
}

impl From<i64> for Version {
    fn from(value: i64) -> Self {
        Self { value }
    }
}

impl ValueObject for Version {}

impl<T> DomainEntityTrait for DomainEntity<T> {
    fn add_event(&mut self, event: DomainEventEnum) {
        if self.events.is_empty() {
            self.version = self.version.next();
        }
        self.events.push(event)
    }
    fn pop_events(&mut self) -> Vec<DomainEventEnum> {
        let res = self.events.clone();
        self.events = vec![];
        res
    }
}

#[enum_dispatch]
pub trait DomainEntityTrait {
    fn add_event(&mut self, event: DomainEventEnum);
    fn pop_events(&mut self) -> Vec<DomainEventEnum>;
}
