use crate::common::types::base::domain_entity::Version;
use crate::shop::domain::menu::meal_description::MealDescription;
use crate::shop::domain::menu::meal_id::MealId;
use crate::shop::domain::menu::meal_name::MealName;
use crate::shop::domain::menu::price::Price;

/// На данный момент эта dto используется в нескольких сценариях.
/// Тут следует быть осторожным и вовремя заметить, когда разным сценариям нужен будет разный набор данных
/// Если такое произойдёт — необходимо выделить отдельный класс. В нашем случае можно оставить и так.
#[derive(Debug, PartialEq)]
pub struct MealInfo {
    pub id: MealId,
    pub name: MealName,
    pub description: MealDescription,
    pub price: Price,
    pub version: Version,
}
