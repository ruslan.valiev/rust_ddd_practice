use crate::shop::usecase::menu::dto::meal_info::MealInfo;

pub trait GetMenu {
    fn execute(&mut self) -> Vec<MealInfo>;
}
