use crate::shop::domain::menu::meal::Meal;
use std::fmt::Debug;

pub trait MealPersister: Debug + Clone {
    fn save(&mut self, meal: Meal);
}
