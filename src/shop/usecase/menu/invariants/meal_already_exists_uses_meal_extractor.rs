use crate::shop::domain::menu::meal_already_exists::MealAlreadyExists;
use crate::shop::domain::menu::meal_name::MealName;
use crate::shop::usecase::menu::access::meal_extractor::MealExtractor;
use derive_new::new;
use std::fmt::Debug;

#[derive(new, Debug, Clone)]
pub struct MealAlreadyExistsUsesMealExtractor<B: MealExtractor> {
    pub extractor: B,
}

impl<B: MealExtractor + Clone> MealAlreadyExists for MealAlreadyExistsUsesMealExtractor<B> {
    fn invoke(&mut self, name: &MealName) -> bool {
        let meal_found_by_get = self.extractor.get_by_name(name.to_owned());
        let meal_found_by_get_is_removed = meal_found_by_get.clone().unwrap_or_default().removed;
        meal_found_by_get.is_some() & !meal_found_by_get_is_removed
    }
}
