use crate::shop::domain::menu::meal_already_exists::MealAlreadyExists;
use crate::shop::domain::menu::test::fixtures::{rnd_meal, rnd_meal_id, rnd_meal_name};
use crate::shop::usecase::menu::invariants::meal_already_exists_uses_meal_extractor::MealAlreadyExistsUsesMealExtractor;
use crate::shop::usecase::test_fixtures::{removed_meal, MockMealExtractor};

#[test]
fn meal_already_exists() {
    let meal = rnd_meal(rnd_meal_id(), false);
    let extractor = MockMealExtractor {
        meal: Some(meal.to_owned()),
        ..MockMealExtractor::default()
    };
    let mut rule = MealAlreadyExistsUsesMealExtractor::new(extractor);

    let result = rule.invoke(&meal.name);

    assert!(result);
    rule.extractor
        .verify_invoked_get_by_name(meal.to_owned().name)
}

#[test]
fn meal_already_exists_but_removed() {
    let meal = removed_meal();
    let extractor = MockMealExtractor {
        meal: Some(meal.to_owned()),
        ..MockMealExtractor::default()
    };
    let mut rule = MealAlreadyExistsUsesMealExtractor::new(extractor);

    let result = rule.invoke(&meal.name);

    assert!(!result);
    rule.extractor.verify_invoked_get_by_name(meal.name);
}

#[test]
fn meal_already_exists_doesnt_exist() {
    let extractor = MockMealExtractor::new();
    let mut rule = MealAlreadyExistsUsesMealExtractor::new(extractor);
    let meal_name = rnd_meal_name();
    let result = rule.invoke(&meal_name);

    assert!(!result);
    rule.extractor.verify_invoked_get_by_name(meal_name);
}
