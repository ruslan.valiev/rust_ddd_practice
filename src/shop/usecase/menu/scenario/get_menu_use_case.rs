use crate::shop::usecase::menu::access::meal_extractor::MealExtractor;
use crate::shop::usecase::menu::dto::meal_info::MealInfo;
use crate::shop::usecase::menu::get_menu::GetMenu;
use derive_new::new;

#[derive(new, Debug)]
pub struct GetMenuUseCase<T: MealExtractor> {
    pub meal_extractor: T,
}

impl<T: MealExtractor> GetMenu for GetMenuUseCase<T> {
    fn execute(&mut self) -> Vec<MealInfo> {
        self.meal_extractor
            .get_all()
            .iter()
            .map(|it| MealInfo {
                id: it.to_owned().id,
                name: it.to_owned().name,
                description: it.to_owned().description,
                price: it.to_owned().price,
                version: it.to_owned().version,
            })
            .collect()
    }
}
