use crate::shop::domain::menu::test::fixtures::{rnd_meal, rnd_meal_id};
use crate::shop::usecase::menu::dto::meal_info::MealInfo;
use crate::shop::usecase::menu::get_meal_by_id::{GetMealById, GetMealByIdUseCaseError};
use crate::shop::usecase::menu::scenario::get_meal_by_id_use_case::GetMealByIdUseCase;
use crate::shop::usecase::test_fixtures::{removed_meal, MockMealExtractor};

#[test]
fn meal_not_found() {
    let meal_extractor = MockMealExtractor::new();
    let mut use_case = GetMealByIdUseCase::new(meal_extractor.to_owned());

    let meal_id = rnd_meal_id();
    let result = use_case.execute(meal_id);

    assert_eq!(result, Err(GetMealByIdUseCaseError::MealNotFound));
    use_case.meal_extractor.verify_invoked_get_by_id(meal_id);
}

#[test]
fn meal_removed() {
    let meal = removed_meal();
    let meal_extractor = MockMealExtractor {
        meal: Option::from(meal.to_owned()),
        ..MockMealExtractor::default()
    };

    let mut use_case = GetMealByIdUseCase::new(meal_extractor);
    let result = use_case.execute(meal.id);

    assert_eq!(result, Err(GetMealByIdUseCaseError::MealNotFound));
    use_case.meal_extractor.verify_invoked_get_by_id(meal.id)
}

#[test]
fn meal_extracted_successfully() {
    let meal = rnd_meal(rnd_meal_id(), false);
    let meal_extractor = MockMealExtractor {
        meal: Option::from(meal.to_owned()),
        ..MockMealExtractor::default()
    };
    let mut use_case = GetMealByIdUseCase::new(meal_extractor);

    let result = use_case.execute(meal.id);
    let meal_info = result;

    assert_eq!(
        meal_info.unwrap(),
        MealInfo {
            id: meal.id,
            name: meal.name,
            description: meal.description,
            price: meal.price,
            version: meal.version,
        }
    );
    use_case.meal_extractor.verify_invoked_get_by_id(meal.id);
}
