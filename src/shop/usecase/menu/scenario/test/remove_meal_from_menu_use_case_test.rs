use crate::shop::domain::menu::test::fixtures::{rnd_meal, rnd_meal_id};
use crate::shop::usecase::menu::remove_meal_from_menu::{
    RemoveMealFromMenu, RemoveMealFromMenuUseCaseError,
};
use crate::shop::usecase::menu::scenario::remove_meal_from_menu_use_case::RemoveMealFromMenuUseCase;
use crate::shop::usecase::test_fixtures::{MockMealExtractor, MockMealPersister};

#[test]
fn successfully_removed() {
    let meal = Some(rnd_meal(rnd_meal_id(), false));
    let cloned_meal = meal.clone().unwrap();
    let meal_persister = MockMealPersister::new();
    let meal_extractor = MockMealExtractor {
        meal: meal.clone(),
        ..MockMealExtractor::default()
    };

    let mut use_case = RemoveMealFromMenuUseCase::new(meal_extractor, meal_persister);
    let result = use_case.execute(cloned_meal.id);

    assert!(result.is_ok());

    use_case.meal_persister.verify_invoked_meal(meal.to_owned());

    use_case
        .meal_extractor
        .verify_invoked_get_by_id(meal.to_owned().unwrap().id);

    use_case
        .meal_persister
        .verify_events_after_deletion(meal.to_owned().unwrap().id);
}

#[test]
fn meal_not_found() {
    let meal_persister = MockMealPersister::new();
    let meal_extractor = MockMealExtractor::new();
    let mut use_case = RemoveMealFromMenuUseCase::new(meal_extractor, meal_persister);

    let meal_id = rnd_meal_id();
    let result = use_case.execute(meal_id);

    assert_eq!(result, Err(RemoveMealFromMenuUseCaseError::MealNotFound));
    use_case.meal_persister.verify_empty();
    use_case.meal_extractor.verify_invoked_get_by_id(meal_id);
}
