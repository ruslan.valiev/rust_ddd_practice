use crate::shop::domain::menu::test::fixtures::{rnd_meal, rnd_meal_id};
use crate::shop::usecase::menu::dto::meal_info::MealInfo;
use crate::shop::usecase::menu::get_menu::GetMenu;
use crate::shop::usecase::menu::scenario::get_menu_use_case::GetMenuUseCase;
use crate::shop::usecase::test_fixtures::MockMealExtractor;

#[allow(non_snake_case)]
#[test]
fn get_menu__menu_is_empty() {
    let meal_extractor = MockMealExtractor::new();
    let mut use_case = GetMenuUseCase::new(meal_extractor);
    let menu = use_case.execute();

    assert!(menu.is_empty());
    use_case.meal_extractor.verify_invoked_get_all();
}

#[test]
fn get_menu() {
    let meal = rnd_meal(rnd_meal_id(), false);
    let meal_extractor = MockMealExtractor {
        meal: Option::from(meal.to_owned()),
        ..MockMealExtractor::default()
    };
    let mut use_case = GetMenuUseCase::new(meal_extractor);
    let menu = use_case.execute();

    assert_eq!(
        menu,
        vec![MealInfo {
            id: meal.id,
            name: meal.name,
            description: meal.description,
            price: meal.price,
            version: meal.version,
        }]
    );
    use_case.meal_extractor.verify_invoked_get_all()
}
