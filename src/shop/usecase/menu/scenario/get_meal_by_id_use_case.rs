use crate::shop::domain::menu::meal_id::MealId;
use crate::shop::usecase::menu::access::meal_extractor::MealExtractor;
use crate::shop::usecase::menu::dto::meal_info::MealInfo;
use crate::shop::usecase::menu::get_meal_by_id::{GetMealById, GetMealByIdUseCaseError};
use derive_new::new;

#[derive(new, Debug)]
pub struct GetMealByIdUseCase<T: MealExtractor> {
    pub meal_extractor: T,
}

impl<T: MealExtractor> GetMealById for GetMealByIdUseCase<T> {
    fn execute(&mut self, id: MealId) -> Result<MealInfo, GetMealByIdUseCaseError> {
        self.meal_extractor.get_by_id(id);
        // dbg!(&self.meal_extractor);
        match self.meal_extractor.get_by_id(id) {
            res if res.is_some() && res.to_owned().unwrap().visible() => Ok(MealInfo {
                id: res.to_owned().unwrap().id,
                name: res.to_owned().unwrap().name,
                description: res.to_owned().unwrap().description,
                price: res.to_owned().unwrap().price,
                version: res.to_owned().unwrap().version,
            }),
            _ => Err(GetMealByIdUseCaseError::MealNotFound),
        }
    }
}
