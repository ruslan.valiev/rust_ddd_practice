use crate::shop::domain::menu::meal::Meal;
use crate::shop::domain::menu::meal_already_exists::MealAlreadyExists;
use crate::shop::domain::menu::meal_description::MealDescription;
use crate::shop::domain::menu::meal_id::{MealId, MealIdGenerator};
use crate::shop::domain::menu::meal_name::MealName;
use crate::shop::domain::menu::price::Price;
use crate::shop::usecase::menu::access::meal_persister::MealPersister;
use crate::shop::usecase::menu::add_meal_to_menu::{AddMealToMenu, AddMealToMenuUseCaseError};
use derive_new::new;
use std::fmt::Debug;
use std::rc::Rc;

#[derive(new, Debug, Clone)]
pub struct AddMealToMenuUseCase<A: MealPersister, C: MealAlreadyExists> {
    pub meal_persister: A,
    id_generator: Rc<dyn MealIdGenerator>,
    meal_exists: C,
}

impl<A, C> AddMealToMenu for AddMealToMenuUseCase<A, C>
where
    A: MealPersister,
    C: MealAlreadyExists,
{
    fn execute(
        &mut self,
        name: MealName,
        description: MealDescription,
        price: Price,
    ) -> Result<MealId, AddMealToMenuUseCaseError> {
        let new_meal_in_menu = Meal::add_meal_to_menu::<C>(
            Rc::clone(&self.id_generator),
            self.clone().meal_exists,
            name,
            description,
            price,
        );
        match new_meal_in_menu {
            Ok(new_meal_in_menu) => {
                self.meal_persister.save(new_meal_in_menu.to_owned());
                Ok(new_meal_in_menu.id)
            }
            Err(_) => Err(AddMealToMenuUseCaseError::AlreadyExists),
        }
    }
}
