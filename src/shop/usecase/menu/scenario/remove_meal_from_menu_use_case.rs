use crate::shop::domain::menu::meal_id::MealId;
use crate::shop::usecase::menu::access::meal_extractor::MealExtractor;
use crate::shop::usecase::menu::access::meal_persister::MealPersister;
use crate::shop::usecase::menu::remove_meal_from_menu::{
    RemoveMealFromMenu, RemoveMealFromMenuUseCaseError,
};
use derive_new::new;

#[derive(new, Debug)]
pub struct RemoveMealFromMenuUseCase<T, U>
where
    T: MealExtractor,
    U: MealPersister,
{
    pub meal_extractor: T,
    pub meal_persister: U,
}

impl<T: MealExtractor, U: MealPersister> RemoveMealFromMenu for RemoveMealFromMenuUseCase<T, U> {
    fn execute(&mut self, id: MealId) -> Result<(), RemoveMealFromMenuUseCaseError> {
        let meal = &self.meal_extractor.get_by_id(id);
        if meal.is_some() {
            let mut unwrapped_meal = meal.to_owned().unwrap();
            unwrapped_meal.remove_meal_from_menu();
            let _ = &mut self.meal_persister.save(unwrapped_meal.to_owned());
            Ok(())
        } else {
            Err(RemoveMealFromMenuUseCaseError::MealNotFound)
        }
    }
}
