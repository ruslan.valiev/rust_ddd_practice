use crate::shop::domain::menu::meal_id::MealId;

pub trait RemoveMealFromMenu {
    fn execute(&mut self, id: MealId) -> Result<(), RemoveMealFromMenuUseCaseError>;
}

#[derive(thiserror::Error, Debug, PartialEq, Clone)]
pub enum RemoveMealFromMenuUseCaseError {
    #[error("Строка не найдена")]
    MealNotFound,
}
