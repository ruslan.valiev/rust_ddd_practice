use crate::shop::domain::menu::meal_id::MealId;
use crate::shop::usecase::menu::dto::meal_info::MealInfo;

pub trait GetMealById {
    fn execute(&mut self, id: MealId) -> Result<MealInfo, GetMealByIdUseCaseError>;
}

#[derive(thiserror::Error, Debug, PartialEq, Clone)]
pub enum GetMealByIdUseCaseError {
    #[error("Еда не найдена")]
    MealNotFound,
}
