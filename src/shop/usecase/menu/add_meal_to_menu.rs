use crate::shop::domain::menu::meal_description::MealDescription;
use crate::shop::domain::menu::meal_id::MealId;
use crate::shop::domain::menu::meal_name::MealName;
use crate::shop::domain::menu::price::Price;

pub trait AddMealToMenu {
    fn execute(
        &mut self,
        name: MealName,
        description: MealDescription,
        price: Price,
    ) -> Result<MealId, AddMealToMenuUseCaseError>;
}

#[derive(thiserror::Error, Debug, Clone, PartialEq)]
pub enum AddMealToMenuUseCaseError {
    #[error("Еда с таким именем уже существует")]
    AlreadyExists,
}
