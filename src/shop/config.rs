use crate::common::types::base::domain_event::{DomainEvent, DomainEventTrait};
#[cfg(test)]
use crate::common::types::base::test::domain_entity_test::TestEvent;
use crate::shop::domain::menu::meal::Meal;
use crate::shop::domain::menu::meal_events::{
    MealAddedToMenuDomainEvent, MealRemovedFromMenuDomainEvent,
};
use crate::DomainEntityTrait;
use enum_dispatch::enum_dispatch;

#[enum_dispatch(DomainEntityTrait)]
#[derive(PartialEq, Serialize, Debug, Clone)]
pub enum DomainEntityEnum {
    Meal(Meal),
}

pub enum DomainEventListenerEnum {}

pub enum DomainEvenPublisherEnum {}

#[enum_dispatch(DomainEventTrait)]
#[derive(PartialEq, Serialize, Debug, Clone, Copy)]
pub enum DomainEventEnum {
    MealRemovedFromMenuDomainEvent,
    MealAddedToMenuDomainEvent,
    DomainEvent,
    #[cfg(test)]
    TestEvent,
}

impl DomainEventEnum {}

// impl DomainEventTrait for DomainEventEnum {}
