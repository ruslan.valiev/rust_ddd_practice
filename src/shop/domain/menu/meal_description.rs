use crate::common::types::base::value_object::ValueObject;
use crate::common::types::errors::error::BusinessError;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize, Default, PartialEq)]
pub struct MealDescription {
    value: String,
}

impl MealDescription {
    pub fn to_string_value(&self) -> &String {
        &self.value
    }

    pub fn new(description: String) -> Result<Self, CreateMealDescriptionError> {
        if description.is_empty() || description == " " {
            Err(CreateMealDescriptionError::EmptyDescriptionError)
        } else {
            Ok(Self { value: description })
        }
    }
}

impl ValueObject for MealDescription {}

#[derive(thiserror::Error, Debug, PartialEq, Clone)]
pub enum CreateMealDescriptionError {
    #[error("Название еды не может быть пустым")]
    EmptyDescriptionError,
}

impl BusinessError for CreateMealDescriptionError {}
