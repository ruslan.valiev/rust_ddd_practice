use crate::common::types::base::domain_entity::Version;
use crate::common::types::common::address::Address;
use crate::shop::domain::menu::meal::Meal;
use crate::shop::domain::menu::meal_description::MealDescription;
use crate::shop::domain::menu::meal_id::MealId;
use crate::shop::domain::menu::meal_name::MealName;
use crate::shop::domain::menu::meal_restorer::MealRestorer;
use crate::shop::domain::menu::price::Price;
use bigdecimal::{BigDecimal, FromPrimitive};
use fake::faker::address::raw::*;
use fake::faker::name::raw::*;
use fake::locales::*;
use fake::Fake;
use rand::thread_rng;
use rand::Rng;

//
// fn address() = Address.from(
// street = faker.address().streetName(),
// building = faker.address().streetAddressNumber().toInt() + 1
// ).getOrElse { error("Address should be right") }

fn _rnd_address() -> Address {
    let building_number_string: String = BuildingNumber(EN).fake();
    Address::new(
        StreetName(EN).fake(),
        building_number_string
            .parse()
            .expect("Address should be right"),
    )
    .unwrap()
}

pub fn rnd_meal_name() -> MealName {
    MealName::new(Name(EN).fake()).unwrap()
}

pub fn rnd_meal_description() -> MealDescription {
    MealDescription::new(Name(EN).fake()).unwrap()
}

pub fn rnd_price() -> Price {
    let random_price: i64 = thread_rng().gen_range(0..500000);
    let price = Price::new(BigDecimal::from_i64(random_price).unwrap()).unwrap();
    Price::new(price.value.with_scale(2)).unwrap()
}

pub fn version() -> Version {
    Version::new()
}

pub fn rnd_meal_id() -> MealId {
    let id: i64 = thread_rng().gen_range(0..i64::MAX);
    MealId { value: id }
}

pub fn rnd_meal(id: MealId, removed: bool) -> Meal {
    MealRestorer::restore_meal(
        id,
        rnd_meal_name(),
        removed,
        rnd_meal_description(),
        rnd_price(),
        version(),
        vec![],
    )
}
//
// fn customerId() = CustomerId(UUID.randomUUID().toString())
//
// fn cartId() = CartId(faker.number().randomNumber())
//
// fn cart(
// meals: Map<MealId, Count> = emptyMap(),
// customerId: CustomerId = customerId(),
// ): Cart {
// return CartRestorer.restoreCart(
// id = cartId(),
// forCustomer = customerId,
// created = OffsetDateTime.now(),
// meals = meals,
// version = version()
// )
// }
//
// fn orderId() = ShopOrderId(faker.number().randomNumber())
//
// fn orderItem(
// price: Price = price(),
// count: Count = count(),
// ): OrderItem {
// return OrderItem(
// meal_id = meal_id(),
// price = price,
// count = count
// )
// }
//
// fn order(
// id: ShopOrderId = orderId(),
// customerId: CustomerId = customerId(),
// state: OrderState = OrderState.COMPLETED,
// orderItems: Set<OrderItem> = setOf(orderItem()),
// ): ShopOrder {
// return ShopOrderRestorer.restoreOrder(
// id = id,
// created = OffsetDateTime.now(),
// forCustomer = customerId,
// orderItems = orderItems,
// address = address(),
// state = state,
// version = version()
// )
// }
