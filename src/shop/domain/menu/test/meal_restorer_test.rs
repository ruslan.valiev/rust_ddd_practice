#![allow(non_snake_case)]

use crate::common::types::base::domain_entity::DomainEntityTrait;
use crate::shop::domain::menu::meal::*;
use crate::shop::domain::menu::meal_restorer::MealRestorer;
use crate::shop::domain::menu::test::fixtures::*;

#[test]
fn restore_meal__success() {
    let mealId = rnd_meal_id();
    let price = rnd_price();
    let name = rnd_meal_name();
    let description = rnd_meal_description();
    let removed = true;
    let version = version();

    let mut meal: Meal = MealRestorer::restore_meal(
        mealId,
        name.clone(),
        removed,
        description.clone(),
        price.clone(),
        version,
        vec![],
    );

    assert_eq!(meal.id, mealId);
    assert_eq!(meal.name, name);
    assert_eq!(meal.description, description);
    assert_eq!(meal.price, price);
    assert_eq!(meal.removed, removed);
    assert_eq!(meal.version, version);

    let popped_events = &meal.pop_events();
    let left_result = serde_json::to_string_pretty(popped_events).unwrap();
    assert_eq!(left_result, "[]")
}
