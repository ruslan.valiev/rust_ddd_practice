#![allow(non_snake_case)]

use crate::common::types::base::domain_entity::DomainEntityTrait;
use crate::shop::config::DomainEventEnum;
use crate::shop::domain::menu::meal::{Meal, MealError};
use crate::shop::domain::menu::meal_already_exists::MealAlreadyExists;

use crate::shop::domain::menu::meal_id::{MealId, MealIdGenerator};
use crate::shop::domain::menu::meal_name::MealName;
use crate::shop::domain::menu::test::fixtures::{
    rnd_meal, rnd_meal_description, rnd_meal_id, rnd_meal_name, rnd_price,
};
use derive_new::new;
use serde_json::to_string_pretty;
use std::rc::Rc;
use std::sync::atomic::AtomicI64;

#[derive(Debug, new, Default)]
pub(crate) struct TestMealIdGenerator {
    #[new(value = "AtomicI64::from(0)")]
    _counter: AtomicI64,
    #[new(value = "rnd_meal_id()")]
    pub meal_id: MealId,
}

impl MealIdGenerator for TestMealIdGenerator {
    fn generate(&self) -> MealId {
        self.meal_id
    }
}

#[derive(Debug, new, Default, Clone, Copy)]
pub struct TestMealAlreadyExists {
    #[new(value = "false")]
    pub value: bool,
}

impl MealAlreadyExists for TestMealAlreadyExists {
    fn invoke(&mut self, _name: &MealName) -> bool {
        self.value
    }
}

#[test]
fn add_meal__success() {
    let id_generator = Rc::new(TestMealIdGenerator::new());
    let meal_exists = TestMealAlreadyExists { value: false };
    let name = rnd_meal_name();
    let description = rnd_meal_description();
    let price = rnd_price();
    let result = Meal::add_meal_to_menu(
        id_generator.to_owned(),
        meal_exists,
        name.to_owned(),
        description.to_owned(),
        price.to_owned(),
    );

    let mut test_meal = result.unwrap();
    assert_eq!(test_meal.to_owned().id, id_generator.meal_id);
    assert_eq!(test_meal.to_owned().name, name);
    assert_eq!(test_meal.to_owned().description, description);
    assert_eq!(test_meal.to_owned().price, price);
    assert!(test_meal.to_owned().visible());

    let popped_events = &test_meal.pop_events();

    let mut popped_events_from_enum = vec![];
    for event in popped_events {
        // let test_unwrap = *event.unwrap();
        match event {
            DomainEventEnum::MealAddedToMenuDomainEvent(event) => {
                popped_events_from_enum.push(event)
            }
            _ => panic!("Событие не того типа"),
        }
    }

    let expected_event = popped_events_from_enum
        .iter()
        .copied()
        .find(|r| r.meal_id == test_meal.id);

    assert_eq!(popped_events_from_enum.first(), expected_event.as_ref());
}

#[test]
fn add_meal_to_menu__already_exists_with_the_same_name() {
    let id_generator = Rc::new(TestMealIdGenerator::new());
    let meal_exists = TestMealAlreadyExists { value: true };
    let price = rnd_price();
    let name = rnd_meal_name();
    let description = rnd_meal_description();
    let result = Meal::add_meal_to_menu(id_generator, meal_exists, name, description, price);

    let left_result = to_string_pretty(&result).unwrap();
    let right_result = to_string_pretty(&Err::<Meal, &MealError>(
        &MealError::AlreadyExistsWithSameNameError,
    ))
    .unwrap();
    assert_eq!(left_result, right_result);
}

#[test]
fn remove_meal_from_menu__success() {
    let mut test_meal = rnd_meal(rnd_meal_id(), false);
    test_meal.remove_meal_from_menu();
    assert!(test_meal.removed);
    assert!(!test_meal.visible());
    let vec_of_popped_enum_events = &test_meal.pop_events();
    let mut popped_events_from_enum = vec![];
    for event in vec_of_popped_enum_events {
        match event {
            DomainEventEnum::MealRemovedFromMenuDomainEvent(event) => {
                popped_events_from_enum.push(event)
            }
            _ => panic!("Событие не того типа"),
        }
    }

    let expected_event = popped_events_from_enum
        .iter()
        .copied()
        .find(|r| r.meal_id == test_meal.id);

    assert_eq!(popped_events_from_enum.first(), expected_event.as_ref());
}

#[test]
fn remove_meal_from_menu__already_removed() {
    let mut meal = rnd_meal(rnd_meal_id(), true);
    meal.remove_meal_from_menu();
    assert!(meal.removed);
    assert!(!meal.visible());

    let left_result = to_string_pretty(&meal.pop_events()).unwrap();
    assert_eq!(left_result, "[]")
}
