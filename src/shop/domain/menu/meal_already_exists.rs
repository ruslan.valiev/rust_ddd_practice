use crate::shop::domain::menu::meal_name::MealName;
use std::fmt::Debug;

pub trait MealAlreadyExists: Debug + Clone {
    fn invoke(&mut self, name: &MealName) -> bool;
}
