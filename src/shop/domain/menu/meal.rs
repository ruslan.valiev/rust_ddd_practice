use super::meal_already_exists::MealAlreadyExists;
use super::meal_description::MealDescription;
use super::meal_events::*;
use super::meal_id::{MealId, MealIdGenerator};
use super::meal_name::MealName;
use super::price::Price;
use crate::common::types::base::domain_entity::{DomainEntityTrait, Version};
use crate::common::types::errors::error::BusinessError;
use crate::shop::config::DomainEventEnum;
use derive_new::new;
use std::rc::Rc;

#[derive(new, Debug, Clone, Default, Serialize, PartialEq)]
pub struct Meal {
    pub id: MealId,
    pub name: MealName,
    pub description: MealDescription,
    pub price: Price,
    pub version: Version,
    #[new(value = "false")]
    pub removed: bool,
    #[new(value = "vec![]")]
    pub events: Vec<DomainEventEnum>,
}

impl Meal {
    pub fn add_meal_to_menu<C: MealAlreadyExists>(
        id_generator: Rc<dyn MealIdGenerator>,
        mut meal_exists: C,
        name: MealName,
        description: MealDescription,
        price: Price,
    ) -> Result<Meal, MealError> {
        if meal_exists.invoke(&name) {
            Err(MealError::AlreadyExistsWithSameNameError)
        } else {
            let id = id_generator.generate();
            let mut meal = Self {
                id,
                name,
                description,
                price,
                version: Version::new(),
                removed: false,
                events: vec![],
            };

            let event_to_be_add: DomainEventEnum = MealAddedToMenuDomainEvent::new(id).into();
            meal.add_event(event_to_be_add);
            Ok(meal)
        }
    }

    pub fn remove_meal_from_menu(&mut self) {
        if !self.to_owned().removed {
            self.removed = true;
            let id_of_removed_meal = self.id;
            self.add_event(MealRemovedFromMenuDomainEvent::new(id_of_removed_meal).into());
        }
    }
    pub fn visible(&self) -> bool {
        !self.removed
    }
}

impl DomainEntityTrait for Meal {
    fn add_event(&mut self, event: DomainEventEnum) {
        if self.events.is_empty() {
            self.version = self.version.next();
        }
        self.events.push(event)
    }
    fn pop_events(&mut self) -> Vec<DomainEventEnum> {
        let res = self.events.clone();
        self.events = vec![];
        res
    }
}

#[derive(thiserror::Error, Debug, PartialEq, Serialize, Deserialize, Clone)]
pub enum MealError {
    #[error("Еда с таким именем уже существует")]
    AlreadyExistsWithSameNameError,
}

impl BusinessError for MealError {}
