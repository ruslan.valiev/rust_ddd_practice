use crate::common::types::base::value_object::ValueObject;
use crate::common::types::errors::error::BusinessError;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize, Default, PartialEq)]
pub struct MealName {
    pub value: String,
}

impl MealName {
    pub fn to_string_value(&self) -> &String {
        &self.value
    }

    pub fn new(name: String) -> Result<Self, CreateMealNameError> {
        if name == *"" || name == *" " {
            Err(CreateMealNameError::EmptyMealNameError)
        } else {
            Ok(Self { value: name })
        }
    }
}

impl ValueObject for MealName {}

#[derive(thiserror::Error, Debug, PartialEq, Clone)]
pub enum CreateMealNameError {
    #[error("Название еды не может быть пустым")]
    EmptyMealNameError,
}

impl BusinessError for CreateMealNameError {}
