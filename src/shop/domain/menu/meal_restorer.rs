use super::meal::Meal;
use super::meal_description::MealDescription;
use super::meal_id::MealId;
use super::meal_name::MealName;
use super::price::Price;
use crate::common::types::base::domain_entity::Version;
use crate::shop::config::DomainEventEnum;

pub struct MealRestorer {}

impl MealRestorer {
    pub fn restore_meal(
        id: MealId,
        name: MealName,
        removed: bool,
        description: MealDescription,
        price: Price,
        version: Version,
        events: Vec<DomainEventEnum>,
    ) -> Meal {
        Meal {
            id,
            name,
            removed,
            version,
            description,
            price,
            events,
        }
    }
}
