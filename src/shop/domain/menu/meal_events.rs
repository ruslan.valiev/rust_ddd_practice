use crate::common::types::base::domain_event::{DomainEventTrait, EventId};
use crate::shop::domain::menu::meal_id::MealId;
use derive_new::new;
use std::fmt::Debug;
use time::OffsetDateTime;

#[derive(new, Debug, PartialEq, Serialize, Deserialize, Clone, Copy)]
pub struct MealAddedToMenuDomainEvent {
    pub meal_id: MealId,
    #[new(value = "EventId::new()")]
    pub id: EventId,
    #[new(value = "OffsetDateTime::now_utc()")]
    pub created: OffsetDateTime,
}

impl DomainEventTrait for MealAddedToMenuDomainEvent {}

#[derive(new, Debug, PartialEq, Serialize, Deserialize, Clone, Copy)]
pub struct MealRemovedFromMenuDomainEvent {
    pub meal_id: MealId,
    #[new(value = "EventId::new()")]
    pub id: EventId,
    #[new(value = "OffsetDateTime::now_utc()")]
    pub created: OffsetDateTime,
}

impl DomainEventTrait for MealRemovedFromMenuDomainEvent {}
