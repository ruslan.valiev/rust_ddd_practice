#![allow(non_snake_case)]

use crate::common::events::domain_event_publisher::DomainEventPublisher;
use crate::shop::domain::menu::meal_events::MealRemovedFromMenuDomainEvent;
use crate::shop::domain::menu::test::fixtures::{rnd_meal, rnd_meal_id, rnd_meal_name};
use crate::shop::in_memory_persistence::menu::in_memory_meal_repository::InMemoryMealRepository;
use crate::shop::in_memory_persistence::menu::test::fixtures::{
    meal_with_remove_event, TestEventPublisher,
};
use crate::shop::usecase::menu::access::meal_extractor::MealExtractor;
use crate::shop::usecase::menu::access::meal_persister::MealPersister;
use mockall::Any;
use std::convert::TryInto;

#[test]
fn saving_meal__meal_doesnt_exist() {
    let event_publisher = TestEventPublisher::new();
    let mut repository = InMemoryMealRepository::new(event_publisher);
    let test_meal = meal_with_remove_event(rnd_meal_id());
    repository.save(test_meal.clone());
    // todo забороть assert_eq!(left_result, right_result);

    let all_events_from_storage = repository.event_publisher.storage;

    assert_eq!(all_events_from_storage.len(), 1_usize);

    let event = all_events_from_storage.first().unwrap().to_owned();
    assert_eq!(
        event.type_name(),
        "rust_ddd_sandbox_new::shop::config::DomainEventEnum"
    );

    let event_struct_from_enum: MealRemovedFromMenuDomainEvent = event.try_into().unwrap();
    let meal_id_from_event = event_struct_from_enum.meal_id;

    let meal_id_from_test_meal = test_meal.id;

    assert_eq!(meal_id_from_test_meal, meal_id_from_event);
}

#[test]
fn saving_meal__meal_exists() {
    let id = rnd_meal_id();
    let existed_meal = rnd_meal(id.to_owned(), false);

    let event_publisher = TestEventPublisher::new();

    let mut repository = InMemoryMealRepository::new(event_publisher);
    repository.storage.insert(existed_meal.id, existed_meal);

    let updatedMeal = meal_with_remove_event(id.to_owned());

    repository.save(updatedMeal.to_owned());

    let event_publisher_events_enum = repository.event_publisher.storage;
    let event_publisher_events: Vec<MealRemovedFromMenuDomainEvent> = event_publisher_events_enum
        .iter()
        .map(|event_enum| event_enum.to_owned().try_into().unwrap())
        .collect();

    let desired_event = event_publisher_events.first().unwrap().to_owned();

    // todo забороть event.shouldBeInstanceOf<MealRemovedFromMenuDomainEvent>()
    assert_eq!(desired_event.meal_id, updatedMeal.id);
}

#[test]
fn get_by_id__meal_exists() {
    let existed_meal = rnd_meal(rnd_meal_id(), false);

    let event_publisher = TestEventPublisher::new();
    let mut repository = InMemoryMealRepository::new(event_publisher);
    repository
        .storage
        .insert(existed_meal.id, existed_meal.to_owned());

    let desired_meal = repository.get_by_id(existed_meal.id).unwrap();

    assert_eq!(desired_meal, existed_meal);
}

#[test]
fn get_by_id__meal_doesnt_exist() {
    let event_publisher = TestEventPublisher::new();
    let mut repository = InMemoryMealRepository::new(event_publisher);
    let meal = repository.get_by_id(rnd_meal_id());
    assert!(meal.is_none())
}

#[test]
fn get_meal_by_name__repository_is_empty() {
    let event_publisher = TestEventPublisher::new();
    let mut repository = InMemoryMealRepository::new(event_publisher);
    let meal = repository.get_by_name(rnd_meal_name());
    assert!(meal.is_none())
}

#[test]
fn get_meal_by_name__success() {
    let stored_meal = rnd_meal(rnd_meal_id(), false);
    let event_publisher = TestEventPublisher::new();
    let mut repository = InMemoryMealRepository::new(event_publisher);
    repository.save(stored_meal.to_owned());

    let meal = repository.get_by_name(stored_meal.name.to_owned());
    assert_eq!(meal.unwrap(), stored_meal);
}

#[test]
fn get_all_meals__repository_is_empty() {
    let event_publisher = TestEventPublisher::new();
    let mut repository = InMemoryMealRepository::new(event_publisher);
    let meals = repository.get_all();
    assert!(meals.is_empty());
}

#[test]
fn get_all_meals__success() {
    let event_publisher = TestEventPublisher::new();
    let mut repository = InMemoryMealRepository::new(event_publisher);
    let stored_meal = rnd_meal(rnd_meal_id(), false);
    repository
        .storage
        .insert(stored_meal.id, stored_meal.to_owned());
    let meals = repository.get_all();
    assert_eq!(meals, vec![stored_meal]);
}

#[test]
fn get_all_meals__removed_is_not_returned() {
    let event_publisher = TestEventPublisher::new();
    let mut repository = InMemoryMealRepository::new(event_publisher);
    let stored_meal = rnd_meal(rnd_meal_id(), true);
    repository.storage.insert(stored_meal.id, stored_meal);

    let meals = repository.get_all();
    assert!(meals.is_empty());
}
