use crate::common::events::domain_event_publisher::DomainEventPublisher;
use crate::common::types::base::domain_event::DomainEventTrait;
use crate::shop::config::DomainEventEnum;
use crate::shop::domain::menu::meal::Meal;
use crate::shop::domain::menu::meal_events::MealRemovedFromMenuDomainEvent;
use crate::shop::domain::menu::meal_id::MealId;
use crate::shop::domain::menu::test::fixtures::rnd_meal;
use derive_new::new;
use enum_dispatch::enum_dispatch;

pub fn meal_with_remove_event(id: MealId) -> Meal {
    let mut meal = rnd_meal(id, false);
    meal.remove_meal_from_menu();
    meal
}

// fn cartWithEvents() -> Cart {
//     let cart = cart();
//     cart.add(meal());
//     cart
// }

// fn orderReadyForComplete(id: ShopOrderId = orderId()) = order(state = OrderState.CONFIRMED, id = id)

// fn orderWithEvents(id: ShopOrderId = orderId()): ShopOrder {
// let order = orderReadyForComplete(id)
//
// check(order.complete() is Either.Right<Unit>)
// return order
// }

#[enum_dispatch(DomainEventTrait)]
#[derive(new, Debug, Serialize, Clone)]
pub enum TestEventPublisherTestEnum {
    TestEventPublisher,
}

#[derive(new, Debug, Serialize, Clone)]
pub struct TestEventPublisher {
    #[new(value = "vec![]")]
    pub storage: Vec<DomainEventEnum>,
}

impl DomainEventPublisher for TestEventPublisher {
    fn publish(&mut self, events: Vec<DomainEventEnum>) {
        for each_event in events {
            self.storage.push(each_event)
        }
    }
}
