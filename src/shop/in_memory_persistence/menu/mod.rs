pub mod in_memory_incremental_meal_id_generator;
pub mod in_memory_meal_repository;
#[cfg(test)]
pub mod test;
