#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate erased_serde;

extern crate serde;
extern crate serde_json;

use crate::common::types::base::domain_event::DomainEvent;
use crate::shop::domain::menu::meal_id::MealId;
use common::types::base::domain_entity::*;
use erased_serde::{Error, Serialize, Serializer};
use std::fmt::Write;
use std::ptr::{addr_of_mut, write};
use std::rc::Rc;

pub mod application;
pub mod common;
pub mod delivery;
pub mod kitchen;
pub mod shop;

use serde_json::{Result, Value};
use time::OffsetDateTime;

fn main() {
    let mut domain_entity = DomainEntity::new(MealId::new(1));
    // println!("Первая распечатка => {:?}", domain_entity);
    let first_event = DomainEvent::new();
    // println!("First Event => {:?}", &first_event);
    let second_event = DomainEvent::new();
    // domain_entity.add_event(Rc::new(first_event));
    // domain_entity.add_event(Rc::new(second_event));

    let data = serde_json::to_string_pretty(&domain_entity).unwrap();

    let v: Value = serde_json::from_str(&data).unwrap();

    // println!("Value => {:?}", v);
    // println!("EVENTS => {}", v["events"][0]);
    // let e = DummyStruct {
    //     a: v["events"][0]["id"]["value"].to_string(),
    // b: OffsetDateTime::try_from(["events"][0]["created"]),
    // };

    // println!("E => {:?}", e);
}

#[derive(Debug)]
pub struct DummyStruct {
    a: String,
    b: OffsetDateTime,
}

fn print_type_of<T>(_: &T) {
    println!("Type is: {}", std::any::type_name::<T>())
}
